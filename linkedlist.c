#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct nodeRec
{
    int item;
    struct nodeRec *next;
} node;

typedef node *nodePtr;

void createList(nodePtr *top)
{
    *top = NULL;
}

nodePtr createNode(int value)
{
    nodePtr newNode = (nodePtr)malloc(sizeof(node));

    if (newNode == NULL)
    {
        return NULL;
    }

    newNode->item = value;
    newNode->next = NULL;

    return newNode;
}

bool addToList(int value, nodePtr *top)
{
    nodePtr newNode = createNode(value);

    if (newNode == NULL)
    {
        return false;
    }

    newNode->next = *top;

    *top = newNode;

    return true;
}

int listSize(nodePtr top)
{
    int count = 0;
    while (top != NULL)
    {
        count++;
        top = top->next;
    }

    return count;
}

void showList(nodePtr top)
{
    while (top != NULL)
    {
        printf("%d\n", top->item);
        top = top->next;
    }
}

void logList(nodePtr top, const char *filename)
{
    FILE *fp = fopen(filename, "a");
    if (fp == NULL)
    {
        printf("Failed to open file %s", filename);
        return;
    }

    fprintf(fp, "%d", (int)time(NULL));
    
    while (top != NULL)
    {
        fprintf(fp, ", %d", top->item);
        top = top->next;
    }

    fprintf(fp, "\n");
    fclose(fp);
}

void deleteList(nodePtr *top)
{
    nodePtr current = *top;
    nodePtr next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }

    *top = NULL;
}
