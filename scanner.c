#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "linkedlist.h"

int main(int argc, char const *argv[])
{
    if (argc != 3)
    {
        printf("ERROR: Expected 2 arguments - min and max port to scan in range.\n");
        return 1;
    }

    int port_min = strtol(argv[1], NULL, 10);
    int port_max = strtol(argv[2], NULL, 10);

    // if either strtol fails
    if (port_min == 0 || port_max == 0)
    {
        printf("Min and max ports must be valid integers.\n");
        return 1;
    }

    // work if min and max are swapped
    if (port_min > port_max)
    {
        int temp = port_min;
        port_min = port_max;
        port_max = temp;
    }

    /* get the url address */
    struct hostent *url = gethostbyname("localhost");
    if (url == NULL)
    {
        fprintf(stderr, "ERROR: Host not found\n");
        return 2;
    }

    /* Initialize socket structure (sockarrd_in) */
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    memcpy(&serv_addr.sin_addr, url->h_addr_list[0], url->h_length);

    nodePtr list;
    createList(&list);

    for (int port = port_min; port <= port_max; port++)
    {
        serv_addr.sin_port = htons(port);

        /* Create a socket */
        int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (sockfd < 0)
        {
            fprintf(stderr, "ERROR: Failed to open socket\n");
            return 3;
        }

        /* Connect to the server */
        if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == 0)
        {
            addToList(port, &list);
        }

        close(sockfd);
    }

    if (listSize(list) == 0)
    {
        printf("No connections established.\n");
    }
    else
    {
        printf("%d connection(s) established on ports:\n", listSize(list));
        showList(list);
    }

    logList(list, "scan.log");

    return 0;
}
