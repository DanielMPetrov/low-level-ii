#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/select.h>

#define MAX_CONNECTIONS 5
#define INVALID_SOCKET -1

// Binds a socket to the given port, and listens for connections
// on success returns the socket file descriptor
// if any steps fails returns -1
int create_bind_listen(int port)
{
    /* Create a socket */
    int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd < 0)
    {
        fprintf(stderr, "ERROR: Failed to open socket\n");
        return -1;
    }

    /* Initialize socket structure (sockarrd_in) */
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serv_addr.sin_port = htons(port);

    /* Bind the host address */
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        fprintf(stderr, "ERROR: bind() failed\n");
        fprintf(stderr, "Error code: %s\n", strerror(errno));
        return -1;
    }

    /* Start listening for the clients (thread blocks) */
    if (listen(sockfd, MAX_CONNECTIONS) < 0)
    {
        fprintf(stderr, "ERROR: listen() failed\n");
        fprintf(stderr, "Error code: %s\n", strerror(errno));
        return -1;
    }

    return sockfd;
}

struct tm *get_current_time()
{
    time_t rawtime;
    time(&rawtime);
    return localtime(&rawtime);
}

void log_console(int port)
{
    static int count = 0;
    printf("%d total hits. Latest connection was on port %d at %s",
           ++count, port, asctime(get_current_time()));
}

void log_file(int port, const char *filename)
{
    FILE *fp = fopen(filename, "a");
    if (fp == NULL)
    {
        printf("Failed opening file %s", filename);
        return;
    }

    fprintf(fp, "Connection detected on port %d at %s", port, asctime(get_current_time()));
    fclose(fp);
}

// https://stackoverflow.com/questions/15560336/listen-to-multiple-ports-from-one-server
int accept_any(int fds[], unsigned int count, struct sockaddr *addr, socklen_t *addrlen, int *index)
{
    *index = -1;

    fd_set readfds;
    FD_ZERO(&readfds);

    int maxfd = -1;
    for (int i = 0; i < count; i++)
    {
        FD_SET(fds[i], &readfds);
        if (fds[i] > maxfd)
        {
            maxfd = fds[i];
        }
    }

    int status = select(maxfd + 1, &readfds, NULL, NULL, NULL);
    if (status < 0)
    {
        return INVALID_SOCKET;
    }

    int fd = INVALID_SOCKET;
    for (int i = 0; i < count; i++)
    {
        if (FD_ISSET(fds[i], &readfds))
        {
            fd = fds[i];
            *index = i;
            break;
        }
    }

    return fd == INVALID_SOCKET ? INVALID_SOCKET : accept(fd, addr, addrlen);
}

int main(int argc, char const *argv[])
{
    if (argc == 1)
    {
#ifdef LITE
        printf("ERROR: Example usage - honeypot-lite <port_ids...>\n");
#else
        printf("ERROR: Example usage - honeypot-full <port_ids...> [<log_file.txt>]\n");
#endif
        return EXIT_FAILURE;
    }

#ifdef LITE
    int count = argc - 1;
    int ports[count];
    for (int i = 1; i < argc; i++)
    {
        ports[i - 1] = strtol(argv[i], NULL, 10);
    }
#else
    const char *filename;
    int count;
    if (strtol(argv[argc - 1], NULL, 10) != 0)
    {
        printf("Warning: missing optional <file> command line argument, defaulting to hits.log\n");
        filename = "hits.log";
        count = argc - 1;
    }
    else
    {
        filename = argv[argc - 1];
        count = argc - 2;
    }
    int ports[count];
    for (int i = 1; i <= count; i++)
    {
        ports[i - 1] = strtol(argv[i], NULL, 10);
    }
#endif

    int descriptors[count];
    for (int i = 0; i < count; i++)
    {
        int sockfd = create_bind_listen(ports[i]);
        if (sockfd < 0)
        {
            return EXIT_FAILURE;
        }
        descriptors[i] = sockfd;
    }

    for (;;)
    {
        /* Accept connection from a client */
        struct sockaddr_in cli_addr;
        socklen_t clilen = sizeof(cli_addr);
        int index = -1;
        int newsockfd = accept_any(descriptors, count, (struct sockaddr *)&cli_addr, &clilen, &index);
        if (newsockfd < 0)
        {
            fprintf(stderr, "ERROR: accept_any() failed\n");
            return EXIT_FAILURE;
        }

        close(newsockfd);

#ifdef LITE
        log_console(ports[index]);
#else
        log_file(ports[index], filename);
#endif
    }

    return EXIT_SUCCESS;
}
