#ifndef LINKEDLIST_HEADER_INCLUDED
#define LINKEDLIST_HEADER_INCLUDED

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct nodeRec
{
    int item;
    struct nodeRec *next;
} node;

typedef node *nodePtr;

void createList(nodePtr *top);
bool addToList(int value, nodePtr *top);
int listSize(nodePtr top);
void showList(nodePtr top);
void logList(nodePtr top, const char *filename);
void deleteList(nodePtr *top);

#endif