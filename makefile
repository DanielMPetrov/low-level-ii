all: clean debug release

release:
	mkdir -p bin/release
	gcc -Wall -o bin/release/honeypot-full honeypot.c
	gcc -Wall -DLITE -o bin/release/honeypot-lite honeypot.c
	gcc -Wall -o bin/release/scanner scanner.c linkedlist.c

debug:
	mkdir -p bin/debug
	gcc -Wall -g -O0 -o bin/debug/honeypot-full honeypot.c
	gcc -Wall -DLITE -g -O0 -o bin/debug/honeypot-lite honeypot.c
	gcc -Wall -g -O0 -o bin/debug/scanner scanner.c linkedlist.c

clean:
	rm -rf bin